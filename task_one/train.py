'''
    Task demo code
'''
from torch.utils.data import Dataset, DataLoader
from torch.optim import Adam
from torch import nn
import torch
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder

import pandas as pd
from tqdm import tqdm


def __read_file(data_path, digit2zero=True):
    """ Read data file of given path.

        :param file_path: path of data file.
        :return: list of sentence, list of slot and list of intent.
    """

    insts = []
    input_seqs = []
    classes = []
    words = []

    with open(data_path, 'r', encoding='utf-8') as f:
        idx = 0
        for line in tqdm(f.readlines()):
            line = line.rstrip()
            items = line.split()
            if line == '':
                input_seqs.append(" ".join(words))
                classes.append(cls)
                cls = []
                words = []
                continue
            if len(items) == 1:
                cls = items[0]
            else:
                words.append(items[0])
    return input_seqs, classes,


train_df = pd.read_csv('train.csv')
dev_df = pd.read_csv('dev.csv')
test_df = pd.read_csv('train.csv')
train_sentence, train_intents = train_df['sentence'], train_df['Category']
dev_sentence, dev_intents = dev_df['sentence'], dev_df['Category']
test_sentence = test_df['sentence']

bow_embedding = CountVectorizer(ngram_range=(1, 1))  # 构建一个词袋 可以自己尝试不同ngram配置
label_encoder = LabelEncoder()  # 将标签转为对应的index，以方便模型学习
# 例如：
# SearchCreativeWork => 1
# PlayMusich => 2
# GetWeather => 3
# find a video game called family dog <=> SearchCreativeWork ->  [0 0 0 0 1 0 0 1 0 0 0 0 1] <=> 1

train_sentence_digit = bow_embedding.fit_transform(train_sentence)  # 将句子向量化
train_target_digit = label_encoder.fit_transform(train_intents)  # 标签数值化

device = torch.device('cuda:0' if torch.cuda.is_available() else "cpu")
train_sentence_digit = torch.tensor(
    train_sentence_digit.todense(), device=device, dtype=torch.float32)
train_target_digit = torch.tensor(train_target_digit, device=device)


dev_sentence_digit = bow_embedding.transform(dev_sentence)
dev_target_digit = label_encoder.transform(dev_intents)
dev_sentence_digit = torch.tensor(
    dev_sentence_digit.todense(), device=device, dtype=torch.float32)
dev_target_digit = torch.tensor(dev_target_digit, device=device)


test_sentence_digit = bow_embedding.transform(test_sentence)
test_sentence_digit = torch.tensor(
    test_sentence_digit.todense(), device=device, dtype=torch.float32)


hidden_size = len(bow_embedding.vocabulary_)
label_num = len(label_encoder.classes_)


class MLP(nn.Module):
    def __init__(self, hidden_size, num_classes, dropout=0.5):
        super(MLP, self).__init__()
        self.Linear1 = nn.Linear(hidden_size, hidden_size // 2)
        self.norm2 = torch.nn.LayerNorm(hidden_size // 2)
        self.relu = torch.nn.LeakyReLU()
        self.drop = torch.nn.Dropout(dropout)
        self.Linear2 = nn.Linear(hidden_size // 2, num_classes)

    def forward(self, inputs):
        x = self.Linear1(inputs)
        x = self.norm2(x)
        x = self.relu(x)
        x = self.drop(x)
        x = self.Linear2(x)
        return x


class MyDataset(Dataset):

    def __init__(self, sentence, labels) -> None:
        super().__init__()
        self.sentence = sentence
        if labels is None:
            labels = [[] for _ in range(len(self.sentence))]
        self.labels = labels

    def __getitem__(self, index: int):
        return self.sentence[index], self.labels[index]

    def __len__(self) -> int:
        return len(self.sentence)


# 构造数据集
dataset = MyDataset(train_sentence_digit, train_target_digit)
train_loader = DataLoader(dataset=dataset, batch_size=32, shuffle=True)

dataset = MyDataset(dev_sentence_digit, dev_target_digit)
dev_loader = DataLoader(dataset=dataset, batch_size=32, shuffle=False)

dataset = MyDataset(test_sentence_digit, None)
test_loader = DataLoader(dataset=dataset, batch_size=32, shuffle=False)

# 训练构造
model = MLP(hidden_size, label_num).to(device)
lr = 0.001  # 可以设置不同的学习率
loss_fn = nn.CrossEntropyLoss()
optimzer = Adam(model.parameters(), lr=lr)   # 可以设置不同的损失函数
num_epoch = 1
best_dev_acc = 0
for epoch in tqdm(range(num_epoch)):
    model = model.train()
    total_loss = 0
    for sentence, labels in tqdm(train_loader):
        sentence = sentence.to(device)
        lables = labels.to(device)
        pred_label = model(sentence)
        optimzer.zero_grad()
        loss = loss_fn(pred_label, labels)
        loss.backward()
        optimzer.step()
        total_loss += loss
    print("Epoch :{}  Total loss: {}".format(epoch, total_loss))
    total_correct = 0
    total_num = 0
    model = model.eval()
    for sentence, labels in tqdm(dev_loader):
        sentence = sentence.to(device)
        lables = labels.to(device)
        pred_label = model(sentence)
        pred_idx = torch.argmax(pred_label, dim=-1).cpu()
        correct_num = sum(pred_idx == labels.cpu())
        batc_num = len(pred_label)
        total_correct += correct_num
        total_num += batc_num
    # 保存最优模型
    acc = total_correct.float() / total_num
    if acc > best_dev_acc:
        torch.save(model, 'best_model.pth')
        best_dev_acc = acc
    print("Epoch :{}  dev acc: {} best_acc: {} ".format(epoch, acc, best_dev_acc))

model = torch.load('best_model.pth')
model = model.to(device)
result = []
# 预测
for sentence, labels in tqdm(test_loader):
    sentence = sentence.to(device)
    pred_label = model(sentence)
    pred_idx = torch.argmax(pred_label, dim=-1)
    result.extend(label_encoder.inverse_transform(pred_idx.cpu()))

with open('result.csv', 'w') as f:
    f.write("Id,Category\n")
    for idx, line in enumerate(result):
        f.write("{},{}\n".format(idx, line))
